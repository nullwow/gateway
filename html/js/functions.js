var doToastr = function (type, head, text, hide = 2000) {
    $.toast({
        heading: head,
        text: text,
        showHideTransition: 'slide',
        icon: type.toLowerCase(),
        position: 'bottom-right',
        stack: false,
        hideAfter: hide
    });
};

var createIssue = function (title, project, description, cb) {
    api.doRequest("POST", `${project}/issues `, { title: title, description: description, labels: 'Gateway'}, cb)
}

var loadBugs = function () {
    $('#content').load('components/content/bugs.html');
}

var manageIssue = function () {
    let classes = $('#issue-submit').attr("class").split(/\s+/);
    if(classes.includes('disabled')){
        return false;
    }
    let description = $('#issue-description').val()
    let name = $('#issue-name').val()
    let email = $('#issue-email').val()
    let title = $('#issue-title').val()

    if(description == '' || name == '' || title == '' || email == '' || !email.includes('@') ) {
        return doToastr('warning', "Erro enviar Issue", "Complete o formulário antes de enviar", 4000)
    }

    let text = `${description}\n\n\n-- Nome: ${name}\n-- Email: ${email}`
    const projects = [9120931, 18732905]

    createIssue(title, projects[$('#issue-project').val()], text, (err, msg) => {
        $('#issue-title').val('')
        $('#issue-name').val('')
        $('#issue-email').val('')
        $('#issue-description').val('')
        if (err) { 
            console.error(err, msg)
            return doToastr('warning', "Erro enviar Issue", "Erro ao enviar issue! Tente novamente mais tarde!", 10000)
        }
        $('issue-submit').removeClass('disabled')
        return doToastr('success', "Issue Criado", "Issue criado com sucesso. Obrigado pela colaboração.", 5000)
    })
    $('issue-submit').addClass('disabled')
}