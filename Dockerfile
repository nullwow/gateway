from nginx:alpine

workdir /usr/share/nginx/html/

COPY ./html/* /usr/share/nginx/html/*

VOLUME /usr/share/nginx/html